In order to run the flask application cd into "flask-server". then type "python server.py" to run the application locally. It will be hosted locally at localhost:5000/. 

To look at the route containing the login information, go to localhost:5000/api/users.

To run the REACT front end, open another terminal and cd into "client" then type "npm start". this should open a new window with the login page.

username = angel
password = password123

typing in the username and password above and clicking the login button will take you to another page with the RESTful services

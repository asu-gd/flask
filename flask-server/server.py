from flask import Flask, jsonify, request, render_template, redirect, url_for
import os

import requests
import xml.etree.ElementTree as ET
#from suds.client import Client

import json
#from pip._vendor import requests

app = Flask(__name__)

response = requests.get('https://venus.sod.asu.edu/WSRepository/Services/WcfRestService4/Service1/PiValue')
#response = 'asd'

aba = {"absval": '-2'}
adda = {"addval": '1',
        "addval2": '3'}

# Members API Route
#@app.route("/members")
#def members():
#    return {"members": ["Member1","Member2","Member3"]}


@app.route('/api/users')
def get_users():
    try:
        with open('users.txt', 'r') as file:
            json_data = file.read()
            users = json.loads(json_data)
            return jsonify({"users": users})
    except FileNotFoundError:
        return jsonify({"error": "File 'users.txt' not found"})


@app.route("/login")
def login():

    return {
        "username":"angel",
        "password":"password123"
    }

@app.route("/pi")
def pi():
    pi = str(response.content)
    c=pi.replace('b\'<double xmlns="http://schemas.microsoft.com/2003/10/Serialization/">','')
    c=c.replace('</double>\'','')
    return {
        "piValue":c
    }

@app.route("/abs")
def abs():
    #data = json.loads(aba)
    function = str('https://venus.sod.asu.edu/WSRepository/Services/WcfRestService4/Service1/AbsValue?x=' + aba['absval'])
    response = str(requests.get(function).content)
    return {
        "method":response
    }

@app.route("/abs/echo/<name>")
def echo(name):
    """echo <name>"""
    #value = request.data_json()
    #aba.insert({
        #'absval': value['absval']
    #})
    function = str('https://venus.sod.asu.edu/WSRepository/Services/WcfRestService4/Service1/AbsValue?x=' + str(name))
    response = str(requests.get(function).content)
    c=response.replace('b\'<int xmlns="http://schemas.microsoft.com/2003/10/Serialization/">','')
    c=c.replace('</int>\'','')
    return {
        "hello": c
    }

#http://venus.sod.asu.edu/WSRepository/Services/WcfRestService4/Service1/add2?x=15&y=17

@app.route("/add")
def add():
    #data = json.loads(aba)
    function = str('http://venus.sod.asu.edu/WSRepository/Services/WcfRestService4/Service1/add2?x=' + adda['addval'] + "&y=" + adda['addval2'])
    response = str(requests.get(function).content)
    return {
        "method":response
    }

@app.route("/add/echo/<name>/<name2>")
def echo2(name,name2):
    """echo <name>"""
    #value = request.data_json()
    #aba.insert({
        #'absval': value['absval']
    #})
    function = str('http://venus.sod.asu.edu/WSRepository/Services/WcfRestService4/Service1/add2?x=' + str(name)+ "&y=" + str(name2))
    response = str(requests.get(function).content)
    c=response.replace('b\'','')
    c=c.replace('\'','')
    return {
        "hello": c
    }

if __name__ == "__main__":
    app.run(debug = True)

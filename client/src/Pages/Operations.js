import React, {useState, useEffect} from 'react' 

const Operations = () => {


    const [data, setData] = useState([{}])
    const [data2, setData2] = useState([{}])

    //pi function
    const handleClick4 = () => {
    
        fetch("/pi").then(
        res => res.json()
        ).then(
            data2 => {
                setData2(data2)
                console.log(data2)
            }
        )
    }

  
    const [data3, setData3] = useState([{}])


    const [data4, setData4] = useState([{}])
    const [abs, setAbs] = useState('')
    const [output2, setOutput2] = useState('')
    const [outputAbs, setOutputAbs2] = useState('')
    const [vas, vassa] = useState('')
    
  
    const handleClick2 = () => {
    

        fetch("/abs/echo/"+ abs).then(
         res => res.json()
        ).then(
            data4 => {
                setOutput2(data4)
                console.log(data4)
            }
        )
    }


  const [addVal1, setaddVal1] = useState('')
  const [addVal2, setaddVal2] = useState('')
  const [pathadd, setpathadd] = useState('')
  const [output3, setoutput3] = useState('')

// add function
  const handleClick3 = () => {
    
    fetch("/add/echo/"+ addVal1 + "/" + addVal2).then(
      res => res.json()
    ).then(
      data5 => {
        setoutput3(data5)
        console.log(data5)
      }
    )
  }





  return (
    <div className="Operations">
      <h1>Welcome to the Operations</h1>

      <p>
        <label for="q">Get pi value:</label>

        {data2.piValue}
      </p>

      <p>
        <button onClick={handleClick4}>Click me</button>
      </p>




      <p>
        <label for="awe">Absolute value:</label>
        {output2.hello}
      </p>

      <textarea
        required
        value={abs}
        onChange={(e) => setAbs(e.target.value)}
      ></textarea>
      <p>
        <button onClick={handleClick2}>Click me</button>
      </p>


      <p>
        <label for="awe">add values:</label>
        {output3.hello}
      </p>

      <textarea
        required
        value={addVal1}
        onChange={(e) => setaddVal1(e.target.value)}
      ></textarea>
      <textarea
        required
        value={addVal2}
        onChange={(e) => setaddVal2(e.target.value)}
      ></textarea>
      <p>
        <button onClick={handleClick3}>Click me</button>
      </p>
    </div>
  );
};

export default Operations;

import React, {useState, useEffect} from 'react' 
import { useNavigate } from 'react-router-dom';

const Home = () => {

    const navigate = useNavigate();

    const [login, setlogin] = useState([{}])

    useEffect(() =>{
        fetch("/api/users").then(
          res => res.json()
        ).then(
          data2 => {
            setlogin(data2)
            console.log(data2.users.usernames)
          }
        )
    }, [])

    const [name, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [output, setOutput] = useState('')

    const handleClick = () => {
    
        if(name == login.users.usernames && password == login.users.passwords){
        //return vassa('-2');
        navigate("/Operations")
        //return setOutput('login was successful');
    }
        //return vassa('-2');
         return setOutput('failed to login');
    }

    return (
        <div className="login-form">
        <p>
        <label for="email">Enter Username:</label>
        </p>
      
        <textarea
        required
        value={name}
        onChange={(e) => setUsername(e.target.value)}
        ></textarea>

        <p>
            <label for="email">Enter Password:</label>
        </p>
      
        <textarea
            required
            value={password}
            onChange={(e) => setPassword(e.target.value)}
        ></textarea>
        <p>
            <button onClick={handleClick}>Login</button>
        </p>
      
        <p>{output}</p>
      
    </div>
  )
}

export default Home;